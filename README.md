# poc_daas

A little PoC for a Desktop-as-a-Service (DaaS) solution.

## Disclaimer

This is still work in progress and not a working prototype. Applying this script will create costs in your aws account and the uninstall script may not remove everything that has been created.

# ToDo

- [ ] Create a role to delete cf stacks automatically after a defined timeframe
- [ ] Finish the CF Stack to setup the workspaces
- [ ] Finish the CF Stack to setup the service catalog

