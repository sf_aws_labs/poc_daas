install:
	@docker run \
		--mount 'type=bind,src=/home/sfinke/Dev/sf_aws_labs/poc_daas,target=/ansible' \
		--mount 'type=bind,src=/home/sfinke/.aws,target=/root/.aws' \
		aws_ansible make _install

uninstall:
	@docker run \
		--mount 'type=bind,src=/home/sfinke/Dev/sf_aws_labs/poc_daas,target=/ansible' \
		--mount 'type=bind,src=/home/sfinke/.aws,target=/root/.aws' \
		aws_ansible make _uninstall
		
_install:
	@ansible-playbook -i ansible/inventory/localhost.yaml --extra-vars "aws_profile=sva" ansible/install.yaml

_uninstall:
	@ansible-playbook -i ansible/inventory/localhost.yaml --extra-vars "aws_profile=sva" ansible/uninstall.yaml